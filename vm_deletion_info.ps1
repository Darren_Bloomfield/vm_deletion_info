#This PS script will show vm's deleted within the last 90 days
#The path to save the file will need to be ammended to a suitable location

$hosts = @(
    "nl1imcvcsa01.mgmt.global",
    "nl2imcvcsa02.mgmt.global",
    "nl2imcvcsa01.mgmt.global"
);
Connect-VIServer -Server $hosts

$start = (Get-Date).AddDays(-90)
Get-VIEvent -Start $start -MaxSamples ([int]::MaxValue) |

where{$_ -is [VMware.Vim.VmRemovedEvent]}| Export-csv -path "C:\a_folder_somewhere\vm_info\vm_deletion_info.csv" -NoTypeInformation    